CPPFLAGS:= -g `sdl2-config --cflags`
LDFLAGS:= `sdl2-config --libs` -lSDL2_ttf -lSDL2_image
CXX:= g++
PROG:= touhou-thing
DESTDIR:=
PREFIX:=

OBJS:= atlasparser.o levelparser.o state.o sprite.o layer.o drawable.o button.o player.o input.o logic.o graphics.o scene.o scenetitle.o sceneoverworld.o scenepause.o game.o main.o

all: $(PROG)

%.o: src/%.cpp src/%.h 
	$(CXX) $(CPPFLAGS) -c $<

#ugly, but solutions include recursive makefiles which is uglier
atlasparser.o: src/io/atlasparser.cpp src/io/atlasparser.h
	$(CXX) $(CPPFLAGS) -c $<

levelparser.o: src/io/levelparser.cpp src/io/levelparser.h
	$(CXX) $(CPPFLAGS) -c $<

state.o: src/containers/state.cpp src/containers/state.h
	$(CXX) $(CPPFLAGS) -c $<

sprite.o: src/containers/sprite.cpp src/containers/sprite.h
	$(CXX) $(CPPFLAGS) -c $<

layer.o: src/containers/layer.cpp src/containers/layer.h
	$(CXX) $(CPPFLAGS) -c $<

drawable.o: src/entities/drawable.cpp src/entities/drawable.h
	$(CXX) $(CPPFLAGS) -c $<

button.o: src/entities/button.cpp src/entities/button.h
	$(CXX) $(CPPFLAGS) -c $<

player.o: src/entities/player.cpp src/entities/player.h
	$(CXX) $(CPPFLAGS) -c $<

input.o: src/subsystems/input.cpp src/subsystems/input.h
	$(CXX) $(CPPFLAGS) -c $<

logic.o: src/subsystems/logic.cpp src/subsystems/logic.h
	$(CXX) $(CPPFLAGS) -c $<

graphics.o: src/subsystems/graphics.cpp src/subsystems/graphics.h
	$(CXX) $(CPPFLAGS) -c $<

scene.o: src/scenes/scene.cpp src/scenes/scene.h
	$(CXX) $(CPPFLAGS) -c $<

scenetitle.o: src/scenes/scenetitle.cpp src/scenes/scenetitle.h
	$(CXX) $(CPPFLAGS) -c $<

sceneoverworld.o: src/scenes/sceneoverworld.cpp src/scenes/sceneoverworld.h
	$(CXX) $(CPPFLAGS) -c $<

scenepause.o: src/scenes/scenepause.cpp src/scenes/scenepause.h
	$(CXX) $(CPPFLAGS) -c $<

$(PROG): $(OBJS)
	$(CXX) -Wall -Wextra -std=c++14 -pedantic -g $(OBJS) -o $(PROG) $(LDFLAGS)

install: all
	@echo installing executable file to ${DESTDIR}${PREFIX}/bin
	@mkdir -p ${DESTDIR}${PREFIX}/bin
	@cp -f ${PROG} ${DESTDIR}${PREFIX}/bin
	@chmod 755 ${DESTDIR}${PREFIX}/bin/${PROG}

uninstall:
	@echo removing executable file from ${DESTDIR}${PREFIX}/bin
	@rm -f ${DESTDIR}${PREFIX}/bin/${PROG}

clean:
	rm -f $(PROG) *.o