#include "layer.h"

Layer::Layer(int rank)
	: rank {rank}, atlas {nullptr}
{}

void Layer::update(State& state, bool pressed_keys[], int& camera_x, int& camera_y)
{
	if (has_player())
		player.update(state, pressed_keys, camera_x, camera_y);

	for (auto& d : drawables)
		d.update(state, pressed_keys, camera_x, camera_y);
}

void Layer::render(SDL_Renderer *renderer)
{
	if (has_atlas()) {
		for (auto& d : drawables)
			d.render(renderer, atlas);
	} else {
		for (auto& d : drawables)
			d.render(renderer);
	}

	if (has_player())
		player.render(renderer);
}