#ifndef LAYER_H
#define LAYER_H

#include <vector>

#include "state.h"
#include "../entities/drawable.h"
#include "../entities/player.h"
#include "SDL2/SDL.h"

class Layer {
public:
	int rank;
	SDL_Texture *atlas;
	Player player;
	std::vector<Drawable> drawables;

	Layer(int rank);
	void update(State& state, bool pressed_keys[], int& camera_x, int& camera_y);
	void render(SDL_Renderer *renderer);

	inline void register_atlas(SDL_Texture *atlas) {
		if (this->atlas == nullptr)
			this->atlas = atlas;
	}

	inline void register_player(SDL_Texture *texture, bool moves) {
		player.texture = texture;
		player.moves = moves;
	}

	inline bool has_atlas() {
		return (atlas != nullptr);
	}

	inline bool has_player() {
		return (player.texture != nullptr);
	}
private:
};

#endif