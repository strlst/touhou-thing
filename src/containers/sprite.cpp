#include "sprite.h"

Sprite::Sprite(int x, int y, int w, int h)
	: src {(SDL_Rect) { x, y, w, h }}
{}

Sprite::Sprite(SDL_Texture *texture)
{
	src.x = 0;
	src.y = 0;
	int w, h;
	SDL_QueryTexture(texture, nullptr, nullptr, &w, &h);
	src.w = w;
	src.h = h;
}

Sprite::Sprite(int x, int y, int w, int h, bool animated, int frames, int current_frame, int duration)
	: src {(SDL_Rect) { x, y, w, h }}, animated {animated}, frames {frames}, current_frame {current_frame}, duration {duration}
{}