#ifndef SPRITE_H
#define SPRITE_H

#include <string>

#include "SDL2/SDL.h"

class Sprite {
public:
	SDL_Rect src;
	bool animated = false;
	int frames = 0;
	int animation_x_offset = 0;
	int current_frame = 0;
	int duration = 0;

	Sprite() = default;
	Sprite(int x, int y, int w, int h);
	Sprite(SDL_Texture *texture);
	Sprite(int x, int y, int w, int h, bool animated, int frames, int current_frame, int duration);
private:
};

#endif