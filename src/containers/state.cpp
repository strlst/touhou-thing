#include "state.h"

State::State(int window_w, int window_h)
	: window_w {window_w}, window_h {window_h}
{}

bool State::consume_bool(bool &b)
{
	bool orig = b;
	if (b)
		b = false;
	return orig;
}