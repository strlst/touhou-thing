#ifndef STATE_H
#define STATE_H

class State {
public:
	State(int window_w, int window_h);

	/* bool */
	bool quit = false;
	bool resized = false;
	bool main_menu_new = false;
	bool main_menu_resume = false;
	bool main_menu_options = false;
	bool last_scene = false;
	bool next_scene = false;
	bool exit_scene = false;
	/* int */
	int window_w;
	int window_h;
	int delta_ms = 0;
	int frame = 0;

	bool consume_bool(bool &b);
private:
};

#endif