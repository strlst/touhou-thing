#include "button.h"

Button::Button(SDL_Texture *texture, bool moves, std::function<void()> callback)
	: Drawable(texture, moves)
{
	this->callback = callback;
}