#ifndef BUTTON_H
#define BUTTON_H

#include <functional>

#include "drawable.h"
#include "../containers/state.h"

class Button : public Drawable {
public:
	Button(SDL_Texture *texture, bool moves, std::function<void()> callback);

	std::function<void()> callback;
private:
};

#endif