#ifndef DRAWABLE_H
#define DRAWABLE_H

#include <vector>

#include "../containers/sprite.h"
#include "../containers/state.h"
#include "SDL2/SDL.h"

enum Vec22 {
	DST = 0, POS, TARGET_ENUM_SIZE
};

class Drawable {
public:
	SDL_Texture *texture;
	SDL_Rect vec22[TARGET_ENUM_SIZE];
	bool moves = false;

	std::vector<Sprite> sprites;
	int selected_sprite = 0;

	Drawable();
	Drawable(SDL_Texture *texture, bool moves);
	Drawable(SDL_Texture *texture, bool moves, Sprite sprite);

	virtual void update(State& state, bool pressed_keys[], int& camera_x, int& camera_y);
	virtual void render(SDL_Renderer *renderer);
	virtual void render(SDL_Renderer *renderer, SDL_Texture *atlas);

	/* utility functions */
	void register_sprite(Sprite sprite);

	/* builder pattern style functions */
	/* arbitrary positioning functions */
	Drawable& set(Vec22 target, SDL_Rect dst);
	Drawable& set(Vec22 target, int x, int y, int w, int h);
	Drawable& set(Vec22 target, Vec22 source);
	Drawable& move(Vec22 target, int x, int y);
	Drawable& move(Vec22 target, Vec22 source);
	Drawable& move_inverted(Vec22 target, int window_w, int window_h, int x, int y);
	/* relative positioning functions */
	Drawable& right_to(Vec22 target, Drawable& other, int border);
	Drawable& below_of(Vec22 target, Drawable& other, int border);
	Drawable& add_x(Vec22 target, int x);
	Drawable& add_y(Vec22 target, int y);
	Drawable& add(Vec22 target, int x, int y);
	/* center functions */
	Drawable& center_x(Vec22 target, int window_w);
	Drawable& center_y(Vec22 target, int window_y);
	Drawable& center(Vec22 target, int window_w, int window_h);
	/* scaling functions */
	Drawable& scale_to_texture();
	Drawable& scale(Vec22 target, int w, int h);
	Drawable& scale(Vec22 target, float f);
	Drawable& scale(Vec22 target, Vec22 source);
protected:
	bool on_screen = true;
};

#endif