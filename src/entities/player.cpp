#include "player.h"
#include "../subsystems/input.h"

Player::Player()
	: Drawable()
{
	this->on_screen = true;
}

Player::Player(SDL_Texture *texture, bool moves)
	: Drawable(texture, moves)
{}

inline void Player::move(int x, int y, int& camera_x, int& camera_y, int window_w, int window_h)
{
	camera_x += x;
	this->vec22[POS].x += x;
	camera_y += y;
	this->vec22[POS].y += y;
	this->vec22[DST].x = window_w / 2 - this->sprites[selected_sprite].src.w / 2;
	this->vec22[DST].y = window_h / 2 - this->sprites[selected_sprite].src.h / 2;
	this->vec22[DST].w = this->sprites[selected_sprite].src.w;
	this->vec22[DST].h = this->sprites[selected_sprite].src.h;
}

void Player::update(State &state, bool pressed_keys[], int& camera_x, int& camera_y)
{
	int x = 0, y = 0;
	y -= pressed_keys[Input::KEY_w] ? 10 : 0;
	x -= pressed_keys[Input::KEY_a] ? 10 : 0;
	y += pressed_keys[Input::KEY_s] ? 10 : 0;
	x += pressed_keys[Input::KEY_d] ? 10 : 0;
	move(x, y, camera_x, camera_y, state.window_w, state.window_h);

	if (pressed_keys[Input::KEY_a])
		selected_sprite = RUNNING_LEFT;
	else if (pressed_keys[Input::KEY_d])
		selected_sprite = RUNNING_RIGHT;
	else if (pressed_keys[Input::KEY_w] || pressed_keys[Input::KEY_s]) {
		if (selected_sprite == IDLE_LEFT)
			selected_sprite = RUNNING_LEFT;
		if (selected_sprite == IDLE_RIGHT)
			selected_sprite = RUNNING_RIGHT;
	} else {
		sprites[selected_sprite].current_frame = 0;
		if (selected_sprite == RUNNING_LEFT)
			selected_sprite = IDLE_LEFT;
		if (selected_sprite == RUNNING_RIGHT)
			selected_sprite = IDLE_RIGHT;
	}
}

void Player::render(SDL_Renderer *renderer)
{
	//SDL_Log("player texture %p with dst %i %i %i %i and src %i %i %i %i and pos %i %i %i %i\n", texture, vec22[DST].x, vec22[DST].y, vec22[DST].w, vec22[DST].h, sprites[selected_sprite].src.x, sprites[selected_sprite].src.y, sprites[selected_sprite].src.w, sprites[selected_sprite].src.h, vec22[POS].x, vec22[POS].y, vec22[POS].w, vec22[POS].h);
	Drawable::render(renderer);
}