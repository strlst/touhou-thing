#ifndef PLAYER_H
#define PLAYER_H

#include "drawable.h"

class Player : public Drawable {
public:
	enum Selector {
		IDLE_RIGHT = 0, IDLE_LEFT, RUNNING_RIGHT, RUNNING_LEFT, SELECTOR_ENUM_SIZE
	};

	Selector sel = IDLE_RIGHT;

	Player();
	Player(SDL_Texture *texture, bool moves);

	void update(State &state, bool pressed_keys[], int& camera_x, int& camera_y) override;
	void render(SDL_Renderer *renderer) override;
private:
	void move(int x, int y, int& camera_x, int& camera_y, int window_w, int window_h);
};

#endif