#ifndef FONT_SELECTOR_H
#define FONT_SELECTOR_H

enum FontSelector {
	FONT_BIG, FONT_MEDIUM, FONT_SMALL
};

#endif