#ifndef SCENE_TYPES_H
#define SCENE_TYPES_H

enum SceneTypes {
	MAIN_MENU = 0,
	OVERWORLD,
	PLAYFIELD,
	SCENE_ENUM_SIZE
};

#endif