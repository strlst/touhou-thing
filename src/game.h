#ifndef GAME_H
#define GAME_H

#include <array>
#include <stack>
#include <memory>

#include "subsystems/input.h"
#include "subsystems/logic.h"
#include "subsystems/graphics.h"
#include "containers/state.h"
#include "scenes/scene.h"

using namespace std;

class Game {
public:
	Game();
	~Game();
	void init();
	int loop();
private:
	/* other modules */
	Input input;
	Logic logic;
	Graphics graphics;

	/* views */
	array<shared_ptr<Scene>, SCENE_ENUM_SIZE> all_scenes;
	stack<shared_ptr<Scene>> active_scenes;

	/* state */
	State state;
};

#endif