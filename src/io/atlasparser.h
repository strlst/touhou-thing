#ifndef ATLAS_PARSER_H
#define ATLAS_PARSER_H

#include <fstream>
#include <iostream>
#include <string>
#include <regex>
#include <vector>

#include "../containers/sprite.h"

enum class AtlasToken {
	INVALID, EMPTY, COMMENT, DESCRIPTOR_DIMENSIONS, DESCRIPTOR_SPRITE, DIMENSIONS, SPRITE, SPRITE_MANY, BREAK
};

class AtlasParser {
public:
	AtlasParser(std::string path);
	~AtlasParser();

	bool dry_run();
	std::vector<Sprite> parse();
private:
	std::ifstream file;
	std::smatch mode;
	const std::vector<std::pair<std::regex, AtlasToken>> matchings {
		{ std::regex(R"(^ *$)"),                                                                             AtlasToken::EMPTY },
		{ std::regex(R"(^#.+$)"),                                                                            AtlasToken::COMMENT },
		{ std::regex(R"(^dimensions$)"),                                                                     AtlasToken::DESCRIPTOR_DIMENSIONS },
		{ std::regex(R"(^(sprite|spritemany)$)"),                                                            AtlasToken::DESCRIPTOR_SPRITE },
		{ std::regex(R"(^(\\+|-)?[[:digit:]]+ (\\+|-)?[[:digit:]]+$)"),                                      AtlasToken::DIMENSIONS },
		{ std::regex(R"(^(false|true) (\\+|-)?[[:digit:]]+ (\\+|-)?[[:digit:]]+$)"),                         AtlasToken::SPRITE },
		{ std::regex(R"(^(\\+|-)?[[:digit:]]+ (false|true) (\\+|-)?[[:digit:]]+ (\\+|-)?[[:digit:]]+$)"),    AtlasToken::SPRITE_MANY },
		{ std::regex(R"(^break$)"),                                                                          AtlasToken::BREAK }
	};

	AtlasToken match_token(std::string line);
};

#endif