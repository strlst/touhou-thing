#include <iostream>
#include <stdio.h>

#include "main.h"
#include "game.h"

int main(void)
{
	Game game;
	game.init();
	return game.loop();
}