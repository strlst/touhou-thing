#include "scene.h"

SceneTypes Scene::get_scene_type()
{
	return type;
}

static bool empty_rect(SDL_Rect s) {
	return (s.x == 0 && s.y == 0 && s.w == 0 && s.h == 0);
}


Drawable& Scene::register_drawable(SDL_Texture *texture, bool moves)
{
	Drawable d = Drawable(texture, moves);
	drawables.push_back(d);
	return drawables.back();
}

Button& Scene::register_button(SDL_Texture *texture, bool moves, std::function<void()> callback)
{
	Button b = Button(texture, moves, callback);
	buttons.push_back(b);
	return buttons.back();
}

std::vector<Drawable>& Scene::get_drawables()
{
	return drawables;
}

std::vector<Button>& Scene::get_buttons()
{
	return buttons;
}