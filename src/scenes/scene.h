#ifndef SCENE_H
#define SCENE_H

#include <vector>

/* forward declarations */
class Input;
class Graphics;

#include "SDL2/SDL.h"
#include "../entities/drawable.h"
#include "../entities/button.h"
#include "../enum/scenetypes.h"
#include "../subsystems/input.h"
#include "../subsystems/graphics.h"

class Scene {
public:
	virtual void init(State& state, Graphics& graphics) = 0;
	virtual void cleanup() = 0;
	virtual void update(State &state, bool pressed_keys[]) = 0;
	virtual void render(SDL_Renderer *renderer) = 0;

	SceneTypes get_scene_type();
	Drawable& register_drawable(SDL_Texture *texture, bool moves);
	Button& register_button(SDL_Texture *texture, bool moves, std::function<void()> callback);
	std::vector<Drawable>& get_drawables();
	std::vector<Button>& get_buttons();
protected:
	SceneTypes type;
	std::vector<Drawable> drawables;
	std::vector<Button> buttons;
};

#endif