#include <string>

#include "sceneoverworld.h"
#include "../containers/sprite.h"
#include "../io/levelparser.h"

SceneOverworld::SceneOverworld(SceneTypes type)
{
	this->type = type;
}

void SceneOverworld::init(State& state, Graphics& graphics)
{
	LevelParser lp { "res/level/overworld.level" };
	if (!lp.dry_run()) {
		SDL_Log("error parsing overworld.level\n");
		exit(EXIT_FAILURE);
	}
	layers = lp.parse(graphics);

	for (auto& layer : layers) {
		SDL_Log("layer of rank %i\n", layer.rank);
		SDL_Log("  has player? %i\n", layer.has_player());
		SDL_Log("  has atlas?  %i\n", layer.has_atlas());
		SDL_Log("  has drawables?  %i drawables\n", layer.drawables.size());
	}
}

void SceneOverworld::cleanup()
{
	for (Drawable& d : drawables)
		if (d.texture != nullptr)
			SDL_DestroyTexture(d.texture);
}

void SceneOverworld::update(State &state, bool pressed_keys[])
{
	if (pressed_keys[Input::KEY_ESCAPE]) {
		pressed_keys[Input::KEY_ESCAPE] ^= 1;
		state.next_scene = true;
	}

	for (auto& layer : layers)
		layer.update(state, pressed_keys, camera_x, camera_y);
}

void SceneOverworld::render(SDL_Renderer *renderer)
{
	for (auto& layer : layers)
		layer.render(renderer);
}