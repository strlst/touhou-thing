#ifndef SCENE_OVERWORLD_H
#define SCENE_OVERWORLD_H

#include "scene.h"
#include "../containers/layer.h"

class SceneOverworld : public Scene {
public:
	SceneOverworld(SceneTypes type);

	void init(State& state, Graphics& graphics);
	void cleanup();
	void update(State &state, bool pressed_keys[]);
	void render(SDL_Renderer *renderer);
private:
	std::vector<Layer> layers;
	int camera_x = 0;
	int camera_y = 0;
};

#endif