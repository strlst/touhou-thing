#include "scenepause.h"

ScenePause::ScenePause(SceneTypes type)
{
	this->type = type;
}

void ScenePause::init(State& state, Graphics& graphics)
{
	/* init default view */
	auto& something = register_drawable(
		graphics.load_png_to_texture("res/img/1607284670491.png"), false
	).scale_to_texture()
	.set(DST, state.window_w / 4, 0, state.window_w / 2, state.window_h);
	something.register_sprite(Sprite(something.texture));

	bool* exit_scene = &state.exit_scene;
	auto& something_else = register_button(
		graphics.load_png_to_texture("res/img/play.png"),
		false,
		[exit_scene](){ *exit_scene = true; }
	).scale_to_texture()
	.set(DST, state.window_w * 0.75, state.window_h / 2, state.window_w / 5, state.window_h / 2);
	something_else.register_sprite(Sprite(something_else.texture));
}

void ScenePause::cleanup()
{
	for (Drawable& d : drawables)
		if (d.texture != nullptr)
			SDL_DestroyTexture(d.texture);
}

void ScenePause::update(State& state, bool pressed_keys[])
{
	if (pressed_keys[Input::KEY_ESCAPE]) {
		pressed_keys[Input::KEY_ESCAPE] ^= 1;
		state.last_scene = true;
	}
}

void ScenePause::render(SDL_Renderer *renderer)
{
	/* draw each drawable in active scene */
	for (Drawable& d : drawables)
		d.render(renderer);

	/* draw each button drawable in active scene */
	for (Button& b : buttons)
		b.render(renderer);
}