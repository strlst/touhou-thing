#ifndef SCENE_PAUSE_H
#define SCENE_PAUSE_H

#include "scene.h"

class ScenePause : public Scene {
public:
	ScenePause(SceneTypes type);

	void init(State& state, Graphics& graphics);
	void cleanup();
	void update(State &state, bool pressed_keys[]);
	void render(SDL_Renderer *renderer);
private:
};

#endif