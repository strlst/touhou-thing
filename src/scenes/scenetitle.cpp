#include "scenetitle.h"

SceneTitle::SceneTitle(SceneTypes type)
{
	this->type = type;
}

void SceneTitle::init(State& state, Graphics& graphics)
{
	/* init default view */
	auto& bg = register_drawable(
		graphics.load_png_to_texture("res/img/1566940044560.png"),
		false
	).scale_to_texture()
	.scale(DST, state.window_w, state.window_h);
	bg.register_sprite(Sprite(bg.texture));

	auto& title = register_drawable(
		graphics.load_text_to_texture(
			"Touhou Thing",
			{ .r = 0xcc, .g = 0x22, .b = 0xff, .a = 0xff },
			FONT_BIG
		),
		false
	).scale_to_texture()
	.center(DST, state.window_w, state.window_h);
	title.register_sprite(Sprite(title.texture));

	bool* main_menu_new = &state.main_menu_new;
	bool* main_menu_resume = &state.main_menu_resume;
	bool* main_menu_options = &state.main_menu_options;
	bool* quit = &state.quit;

	auto& button_new = register_button(
		graphics.load_text_to_texture(
			"New Game",
			{ .r = 0x99, .g = 0x99, .b = 0xff, .a = 0xff },
			FONT_SMALL
		),
		false,
		[main_menu_new](){ *main_menu_new = true; }
	).scale_to_texture()
	.move(DST, state.window_w * 0.02, state.window_h * 0.02);
	button_new.register_sprite(Sprite(button_new.texture));

	auto& button_resume = register_button(
		graphics.load_text_to_texture(
			"Resume Game",
			{ .r = 0x99, .g = 0x99, .b = 0xff, .a = 0xff },
			FONT_SMALL
		),
		false,
		[main_menu_resume](){ *main_menu_resume = true; }
	).scale_to_texture()
	.below_of(DST, button_new, 0);
	button_resume.register_sprite(Sprite(button_resume.texture));

	auto& button_options = register_button(
		graphics.load_text_to_texture(
			"Options",
			{ .r = 0x99, .g = 0x99, .b = 0xff, .a = 0xff },
			FONT_SMALL
		),
		false,
		[main_menu_options](){ *main_menu_options = true; }
	).scale_to_texture()
	.below_of(DST, button_resume, 0);
	button_options.register_sprite(Sprite(button_options.texture));

	auto& button_quit = register_button(
		graphics.load_text_to_texture(
			"Quit",
			{ .r = 0x99, .g = 0x99, .b = 0xff, .a = 0xff },
			FONT_SMALL
		),
		false,
		[quit](){ *quit = true; }
	).scale_to_texture()
	.move_inverted(DST, state.window_w, state.window_h, state.window_w * 0.02, state.window_h * 0.02);
	button_quit.register_sprite(Sprite(button_quit.texture));
}

void SceneTitle::cleanup()
{
	for (Drawable& d : drawables)
		if (d.texture != nullptr)
			SDL_DestroyTexture(d.texture);
}

void SceneTitle::update(State& state, bool pressed_keys[])
{}

void SceneTitle::render(SDL_Renderer *renderer)
{
	/* draw each drawable in active scene */
	for (Drawable& d : drawables)
		d.render(renderer);

	/* draw each button drawable in active scene */
	for (Button& b : buttons)
		b.render(renderer);
}