#ifndef SCENE_TITLE_H
#define SCENE_TITLE_H

#include "scene.h"

class SceneTitle : public Scene {
public:
	SceneTitle(SceneTypes type);

	void init(State& state, Graphics& graphics);
	void cleanup();
	void update(State& state, bool pressed_keys[]);
	void render(SDL_Renderer *renderer);
private:
};

#endif