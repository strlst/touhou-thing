#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <string>
#include <stack>
#include <memory>

/* forward declarations */
class Scene;

#include "../scenes/scene.h"
#include "../entities/drawable.h"
#include "../containers/state.h"
#include "../enum/fontselector.h"
#include "SDL2/SDL.h"
#include "SDL2/SDL_ttf.h"

using namespace std;

class Graphics {
public:
	/* rendering functions */
	void init(int window_w, int window_h);
	void cleanup();
	void render(State& state, stack<shared_ptr<Scene>>& active_scenes);

	/* texture creation */
	SDL_Texture *load_png_to_texture(string path);
	SDL_Texture *load_text_to_texture(string text, SDL_Color color, FontSelector sel);
	SDL_Texture *load_text_to_texture(string text, SDL_Color color);
private:
	/* constants */
	const char *WINDOW_TITLE = "touhou-thing";

	/* sdl state */
	SDL_Window *window;
	SDL_Renderer *renderer;
	SDL_Texture *display;
	TTF_Font *font_big;
	TTF_Font *font_medium;
	TTF_Font *font_small;
};

#endif