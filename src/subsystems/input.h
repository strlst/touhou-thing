#ifndef INPUT_H
#define INPUT_H

#include <memory>

/* forward declarations */
class Scene;

#include "../entities/button.h"
#include "../containers/state.h"
#include "../scenes/scene.h"

class Input {
public:
	enum Key {
		KEY_UNIMPLEMENTED = 0,
		KEY_RETURN, KEY_ESCAPE,
		KEY_w, KEY_a, KEY_s, KEY_d,
		KEY_h, KEY_j, KEY_k, KEY_l,
		KEYS_ENUM_SIZE
	};

	bool pressed_keys[KEYS_ENUM_SIZE];

	void query(State& state, std::shared_ptr<Scene> active_scene);
private:
	static Key key_lookup(int keycode);
};

#endif