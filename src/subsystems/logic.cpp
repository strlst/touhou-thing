#include "logic.h"
#include "input.h"

void Logic::update(State &state, array<shared_ptr<Scene>, SCENE_ENUM_SIZE>& all_scenes, stack<shared_ptr<Scene>>& active_scenes, bool pressed_keys[])
{
	shared_ptr<Scene>& active_scene = active_scenes.top();

	active_scene->update(state, pressed_keys);

	SceneTypes type = active_scene->get_scene_type();
	switch (type) {
		case MAIN_MENU:
			if (state.consume_bool(state.main_menu_resume)) {
				/* create next scene */
				active_scenes.push(all_scenes[OVERWORLD]);
			}
			break;
		case OVERWORLD:
			if (state.consume_bool(state.next_scene)) {
				active_scenes.push(all_scenes[PLAYFIELD]);
			}
			break;
		default:
			if (state.consume_bool(state.last_scene)) {
				active_scenes.pop();
			}

			if (state.consume_bool(state.exit_scene)) {
				active_scenes.pop();
				active_scenes.pop();
			}
			break;
	}
}