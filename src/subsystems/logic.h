#ifndef LOGIC_H
#define LOGIC_H

#include <array>
#include <stack>
#include <memory>

#include "../containers/state.h"
#include "../scenes/scene.h"

using namespace std;

class Logic {
public:
	void update(State &state, array<shared_ptr<Scene>, SCENE_ENUM_SIZE>& all_scenes, stack<shared_ptr<Scene>>& active_scenes, bool pressed_keys[]);
private:
};

#endif